import * as FS from "fs"
import * as Path from "path"
import { isBinaryFileSync } from "isbinaryfile"
import { sync as makeDirSync } from "make-dir"
import { sync as delSync } from "del"

const d = <T>(x: T) => x

/**
 * Lists files from a directory.
 */
export const dir = function(dir: string) {
  return FS.readdirSync(dir, { encoding: "utf8" })
}
export const ls = dir

/**
 * Returns the contents of the input files.
 */
export function cat(...files: (string | string[])[]) {
  return files.flatMap(d).map(file => {
    const filePath = Path.resolve(file)
    if (isBinaryFileSync(filePath)) {
      return FS.readFileSync(file)
    } else {
      return FS.readFileSync(file, { encoding: "utf8" })
    }
  })
}

export function mkdir(...dirs: (string | string[])[]) {
  return dirs.flatMap(d).map(dir => {
    makeDirSync(dir)
  })
}

export const md = mkdir

export function del(...patterns: (string | string[])[]) {
  return delSync(patterns.flatMap(d))
}
