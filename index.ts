#!/usr/bin/env node

import { start, ReplOptions } from "repl"
import * as globals from "./globals"

function startRepl(startOpts?: ReplOptions) {
  const repl = start({
    ...startOpts
  })

  for (const [key, value] of Object.entries(globals)) {
    Object.defineProperty(repl.context, key, {
      configurable: false,
      enumerable: true,
      value
    })
  }
}

if (require.main === module) {
  startRepl()
}
